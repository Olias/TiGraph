﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TIGraphWPF.Support;

namespace TIGraphWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            FileHandler = new FileHandler();
            InitializeComponent();
        }

        public FileHandler FileHandler { get; set; }
        public DeinzerFileCompiler Comp = new DeinzerFileCompiler();
        public string MainTextBoxText { get; set; }
        private async void Initalize()//TODO Put in Model
        {
            MainTextBoxText = await FileHandler.ReadFile();
            var graph = Comp.TranslateFile(MainTextBoxText);
            var test2=graph.berechneSpannbaum("A");
            //var test=graph.BreitensucheEinGraph();
            var test=graph.DijkstraShortestPath("A", "D");
            await FileHandler.WritePicker(graph.ConvertToDot());

        }
        public void StartButtonTapped(object sender, RoutedEventArgs e)
        {
            Initalize();
            Comp.TranslateFile(MainTextBoxText);
        }

        
    }
}
