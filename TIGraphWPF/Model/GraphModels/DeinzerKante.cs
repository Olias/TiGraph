﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGraphWPF.Model.GraphModels
{
    public class DeinzerKante:IComparable<DeinzerKante>
    {
        public DeinzerKnoten Start { get; set; }
        public DeinzerKnoten End { get; set; }
        public int Value { get; set; } = -1;

        public DeinzerKante(DeinzerKnoten _start, DeinzerKnoten _end, int _value)
        {
            Start = _start;
            End = _end;
            Value = _value;
        }
        public DeinzerKante(DeinzerKnoten _start, DeinzerKnoten _end)
        {
            Start = _start;
            End = _end;
        }

        public DeinzerKante(DeinzerKante Kante,bool reverse)
        {
            if (!reverse)
            {
              Start = Kante.Start;
            End = Kante.End;  
            }
            else
            {
                Start = Kante.End;
                End = Kante.Start;
            }
            
            Value = Kante.Value;
        }
        public int CompareTo(DeinzerKante other)
        {
            return String.Compare(this.Start.Name, other.Start.Name);
        }
    }
}
