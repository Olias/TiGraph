﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using TIGraphWPF.Support;

namespace TIGraphWPF.Model.GraphModels
{
    public class DeinzerGraph
    {
        //public List<DeinzerKnoten> Knoten { get; set; }
        //public List<DeinzerKante> Kanten { get; set; }
        //public bool Directed { get; set; } = false;
        //public int MyProperty { get; set; }
        //schreiben indem "prop" dann doppel tab
        public DeinzerGraph()
        {
            Knoten = new List<DeinzerKnoten>();
            Kanten = new List<DeinzerKante>();
        }

        public DeinzerGraph(ConcurrentBag<DeinzerKnoten> _knoten, ConcurrentBag<DeinzerKante> _kanten)
        {
            Knoten = new List<DeinzerKnoten>();
            Kanten = new List<DeinzerKante>();
            foreach (var item in _knoten)
                Knoten.Add(item);
            foreach (var item in _kanten)
                Kanten.Add(item);
        }

        public DeinzerGraph(List<DeinzerKnoten> _knoten, List<DeinzerKante> _kanten)
        {
            Knoten = new List<DeinzerKnoten>();
            Kanten = new List<DeinzerKante>();
            foreach (var item in _knoten)
                Knoten.Add(item);
            foreach (var item in _kanten)
                Kanten.Add(item);
        }

        public List<DeinzerKnoten> Knoten { get; set; }
        public List<DeinzerKante> Kanten { get; set; }
        public bool Directed { get; set; } = false;

        private string Pre
        {
            get
            {
                if (Directed)
                    return "digraph {";
                return "graph {";
            }
        }

        private string Mid
        {
            get
            {
                var temp = "";
                foreach (var item in Kanten)
                    if (Directed)
                        temp += MidElement(item, "->");
                    else
                        temp += MidElement(item, "--");
                return temp;
            }
        }

        private string After => "}";

        public string ConvertToDot()
        {
            //Kanten.Sort();
            return Pre + Mid + After;
        }

        private string MidElement(DeinzerKante kante, string seperator)
        {
            if (kante.Value == -1)
                return kante.Start.Name + seperator + kante.End.Name;
            return kante.Start.Name + seperator + kante.End.Name + "[label=\"" + kante.Value + "\"];";
        }

        public bool UndirectedCyrcleSearch()
        {
            var tempInitList = Knoten;
            var tempint = tempInitList.Count;
            var tempout = new List<DeinzerKnoten>
            {
                tempInitList.First()
            };
            for (var i = 0; i < tempint; i++)
            {
                if (tempout.Count <= i)
                {
                    var temp = 0;
                    do
                    {
                        if (tempout.Find(a => a.Name == Knoten[temp].Name) == null)
                            tempout.Add(Knoten[temp]);
                        temp++;
                    } while (tempout.Count != i + 1);
                }
                var index = tempout[i];
                foreach (var item in tempout[i].Kanten)
                    if (item.Start != index)
                    {
                        if (tempout.Contains(item.Start))
                            return true;
                        tempout.Add(item.Start);
                        var _remove = tempInitList.FindIndex(a => a == item.Start);
                        tempInitList[_remove].Kanten.Remove(item);
                    }
                    else
                    {
                        if (tempout.Contains(item.End))
                            return true;
                        tempout.Add(item.End);
                        var _remove = tempInitList.FindIndex(a => a == item.End);
                        tempInitList[_remove].Kanten.Remove(item);
                    }

                tempInitList.Remove(tempout[i]);
            }
            return false;
        }

        public bool BreitensucheEinGraph()
        {
            var tempInitList = new List<DeinzerKnoten>(Knoten);
            var tempint = tempInitList.Count;
            var tempout = new List<DeinzerKnoten>
            {
                tempInitList.First()
            };
            for (var i = 0; i < tempint; i++)
            {
                if (tempout.Count <= i)
                    return false;
                var index = tempout[i];
                foreach (var item in tempout[i].Kanten)
                    if (item.Start != index)
                    {
                        tempout.Add(item.Start);
                        var _remove = tempInitList.FindIndex(a => a == item.Start);
                        tempInitList[_remove].Kanten.Remove(item);
                    }
                    else
                    {
                        tempout.Add(item.End);
                        var _remove = tempInitList.FindIndex(a => a == item.End);
                        tempInitList[_remove].Kanten.Remove(item);
                    }

                tempInitList.Remove(tempout[i]);
            }
            return true;
        }

        /// <summary>
        ///     returns a list with the points in order first visted
        /// </summary>
        /// <returns></returns>
        public List<DeinzerKnoten> UndirectedCyrcleSearchWithColor()
        {
            var tempInitList = Knoten;
            var tempint = tempInitList.Count;

            var tempout = new List<DeinzerKnoten>
            {
                tempInitList.First()
            };
            for (var i = 0; i < tempint; i++)
            {
                if (tempout.Count <= i)
                {
                    var temp = 0;
                    do
                    {
                        if (tempout.Find(a => a.Name == Knoten[temp].Name) == null)
                            tempout.Add(Knoten[temp]);
                        temp++;
                    } while (tempout.Count != i + 1);
                }
                var index = tempout[i];
                foreach (var item in tempout[i].Kanten)
                    if (item.Start != index)
                    {
                        if (tempout.Contains(item.Start))
                            return tempout;
                        tempout.Add(item.Start);
                        var _remove = tempInitList.FindIndex(a => a == item.Start);
                        tempInitList[_remove].Kanten.Remove(item);
                    }
                    else
                    {
                        if (tempout.Contains(item.End))
                            return tempout;
                        tempout.Add(item.End);
                        var _remove = tempInitList.FindIndex(a => a == item.End);
                        tempInitList[_remove].Kanten.Remove(item);
                    }

                tempInitList.Remove(tempout[i]);
            }
            return tempout;
        }

        public void test(List<DeinzerKante> input)
        {
        }

        private bool checkEvenDegree()
        {
            var temp = false;

            if (Directed)
            {
                foreach (var k in Knoten)
                    if (k.InputDegree == k.OutputDegree)
                        temp = true;
                    else
                        return false;
                return temp;
            }
            foreach (var k in Knoten)
                if (k.Degree % 2 == 0)
                    temp = true;
                else
                    return false;
            return temp;
        }

        public IEnumerable<DeinzerKnoten> BreadthFirstSearch(DeinzerKnoten k)
        {
            var deinzerKnotenQueue = new Queue<DeinzerKnoten>();
            var visitedDeinzerKnoten = new HashSet<DeinzerKnoten>();

            deinzerKnotenQueue.Enqueue(k);

            while (deinzerKnotenQueue.Count != 0)
            {
                var currentNode = deinzerKnotenQueue.Dequeue();
                if (visitedDeinzerKnoten.Contains(currentNode))
                    continue;

                visitedDeinzerKnoten.Add(currentNode);
                yield return currentNode;

                deinzerKnotenQueue.EnqueueRange(currentNode.getNeighbours().Except(visitedDeinzerKnoten));
            }
        }

        public List<DeinzerKante> berechneSpannbaum(string start)
        {
            List<DeinzerKnoten> usedKnoten = new List<DeinzerKnoten>(); // Liste mit derzeit aktiven Knoten
            List<DeinzerKante> usedKanten = new List<DeinzerKante>(); // Liste mit Kanten die nicht benutzt oder verworfen wurden
            // Klassenvariable Knoten : Alle Knoten des Graphen
            // Klassenvarible Kanten : Alle Kanten des Graphen
            List<DeinzerKante> usableKanten = new List<DeinzerKante>();

            usedKnoten.Add(Knoten.First(a => a.Name==start));

            while(usedKnoten.Count < this.Knoten.Count)
            {
                foreach(DeinzerKnoten kno in usedKnoten)
                {
                    foreach(DeinzerKante kan in kno.Kanten)
                    {
                        //if (usableKanten.Contains(kan))
                        //    usableKanten.Remove(kan);
                        //else if (!usedKanten.Contains(kan))
                        //    usableKanten.Add(kan);
                        if (kan.Start.Name == kno.Name && !usedKnoten.Contains(kan.End))
                            usableKanten.Add(kan);
                        if (kan.End.Name == kno.Name && !usedKnoten.Contains(kan.Start))
                            usableKanten.Add(kan);
                    }
                }
                var temp = usableKanten.OrderBy(a => a.Value).First();
                usedKanten.Add(temp);
                if (usedKnoten.Contains(temp.Start))
                    usedKnoten.Add(temp.End);
                else
                    usedKnoten.Add(temp.Start);

                usableKanten.Clear();
            }
            return usedKanten;
        }
    
        
    




        public List<DeinzerKnoten> DijkstraShortestPath(string start, string end)
        {
            var visited = new List<DijKnoten>();
            var notVisited = Knoten.Select(item => new DijKnoten(item, Directed)).ToList();
            notVisited.First(a => a.Name == start).Distance = 0;//set the first Knoten Distance to 0
            for (var i = 0; i < Knoten.Count; i++)
            {
                //set the current Knoten to the one not fisited with the lowest distance
                var currentDijKnoten = notVisited.Where(a => a.Distance != -1).OrderBy(a => a.Distance).First();
                visited.Add(currentDijKnoten);
                notVisited.Remove(currentDijKnoten);
                //update for every Knoten which is used the distance if it is lower than before
                foreach (var kante in currentDijKnoten.Kanten)
                {
                    if (visited.Any(a => a.Name == kante.End.Name)) continue;
                    {
                        var secondKnoten = notVisited.First(a => a.Name == kante.End.Name);
                        var distance = kante.Value + currentDijKnoten.Distance;
                        if (distance > secondKnoten.Distance && secondKnoten.Distance != -1) continue; 
                        secondKnoten.Distance = distance;
                        secondKnoten.Previous = currentDijKnoten;
                    }
                }
                if (currentDijKnoten.Name == end)
                    return currentDijKnoten.ReturnDeinzerKnotens();
            }
            return new List<DeinzerKnoten>();
        }
    }
}
