﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGraphWPF.Model.GraphModels
{
    public class DijKnoten:DeinzerKnoten
    {
        public int Distance { get; set; } = -1;
        public DijKnoten Previous { get; set; } = null;
        public DijKnoten(DeinzerKnoten knoten, bool directed)
        {
            if (!directed)
            {
                foreach (var kante in knoten.Kanten)
                {
                    Kanten.Add(kante.Start.Name == knoten.Name
                        ? new DeinzerKante(kante, false)
                        : new DeinzerKante(kante, true));
                }
            }
            else
            {
                foreach (var kante in knoten.Kanten)
                {
                    if (kante.Start.Name==knoten.Name)
                    {
                        Kanten.Add(new DeinzerKante(kante, false));
                    }                    
                }
            }
            
            this.Name = knoten.Name;
        }

        public DijKnoten()
        {
            
        }

        public List<DeinzerKnoten> ReturnDeinzerKnotens()
        {
            var returnList = new List<DeinzerKnoten>();
            if (Previous == null)
            {
                returnList.Add(this);
                return returnList;
            }
            else
            {
                returnList = Previous.ReturnDeinzerKnotens();
                returnList.Add(this);
                return returnList;
            }
        }
    }
}
