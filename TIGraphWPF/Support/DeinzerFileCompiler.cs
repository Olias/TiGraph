﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TIGraphWPF.Model.GraphModels;

namespace TIGraphWPF.Support
{
    public class DeinzerFileCompiler
    {
        //public ConcurrentBag<DeinzerKnoten> Nodes { get; set; }
        //public ConcurrentBag<DeinzerKante> Edges { get; set; 
        List<DeinzerKnoten> Nodes { get; set; }
        public List<DeinzerKante> Edges { get; set; }
        char[] Signs = { '#'};
        string[] stringSeparators = new string[] { "\r\n" };
        public DeinzerFileCompiler()
        {
            //Nodes = new ConcurrentBag<DeinzerKnoten>();
            //Edges = new ConcurrentBag<DeinzerKante>();
            Nodes = new List<DeinzerKnoten>();
            Edges = new List<DeinzerKante>();
        }

        public DeinzerGraph TranslateFile(string inputString)
        {
            inputString=RemoveBetween(inputString,"#", "\r\n");
            var inputStrings=inputString.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
            //Todo 
            //Parallel.ForEach(inputStrings, singleString => {
            //    FilterObjects(singleString);
            //});
            if (inputStrings.Count()==1)//Todo abfangen von einen String
            {

            }
            foreach (var item in inputStrings)
            {
                FilterObjects(item);
            }
            return new DeinzerGraph(Nodes,Edges);
        }
        public string RemoveBetween(string s, string begin, string end)
        {
            Regex regex = new Regex(string.Format("\\{0}.*?\\{1}", begin, end));
            return regex.Replace(s, string.Empty);
        }
        private void FilterObjects(string inputString)//Todo
        {
            inputString = inputString.Replace("\r\n","");
            var words = inputString.Split(' ');
            if (words.Count() != 0)
            {
                switch (words[0])
                {
                    case "knoten":
                        {
                            Nodes.Add(new DeinzerKnoten(words[1]));
                            break;
                        }
                    case "kante":
                        {
                            var node1 = FindKnotenByValue(words[1]);
                                var node2 = FindKnotenByValue(words[2]);
                            var index1=Nodes.FindIndex(a => a == node1);
                            var index2 = Nodes.FindIndex(a => a == node2);
                            switch (words.Count())
                            {
                                case 3:
                                    {
                                        var Edge = new DeinzerKante(node1, node2);
                                Edges.Add(Edge);
                                Nodes[index1].Kanten.Add(Edge);
                                Nodes[index2].Kanten.Add(Edge);
                                        break;
                                    }
                                case 4:
                                    {
                                        words[3].Replace("  ", string.Empty);
                                if (words[3]=="")
                                {
                                            goto case 3;
                                }
                                try
                                {
                                    var Edge = new DeinzerKante(node1, node2, int.Parse(words[3]));
                                    Edges.Add(Edge);
                                    Nodes[index1].Kanten.Add(Edge);
                                    Nodes[index2].Kanten.Add(Edge);
                                }
                                catch (Exception)
                                {
                                }
                                        break;
                                    }
                                default:
                                    break;
                            }
                            break;
                        }
                    default:
                        break;
                }
            }
        }
        private DeinzerKnoten FindKnotenByValue(string value) => Nodes.Find(item => item.Name == value);
    }
}
