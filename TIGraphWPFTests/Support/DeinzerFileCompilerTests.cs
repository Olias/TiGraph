﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TIGraphWPF.Support;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGraphWPF.Support.Tests
{
    [TestClass()]
    public class DeinzerFileCompilerTests
    {
        [TestMethod()]
        public void TranslateFileFullGraph()
        {
            DeinzerFileCompiler Comp = new DeinzerFileCompiler();
            var test = Comp.TranslateFile("# 9 Knotendefinitionen, A bis I knoten A knoten B knoten C knoten D knoten E # Kantendefinitionen kante B C kante B E kante A B");
            Assert.IsNotNull(test);
        }
        [TestMethod()]
        public void TranslateFileMoreThanOneGraphs()
        {
            DeinzerFileCompiler Comp = new DeinzerFileCompiler();
            var test = Comp.TranslateFile("# 9 Knotendefinitionen, A bis I /r/n knoten A /r/n knoten B /r/n knoten C /r/n knoten D /r/n knoten E /r/n # Kantendefinitionen /r/n kante B C /r/n kante B E /r/n");
            if (test.Kanten.Count() > 2)
            {
                Assert.Fail();
            }
            if (test.Knoten.Count() != 0)
            {
                Assert.Fail();
            }
        }
        [TestMethod()]
        public void TranslateFileNotConnectedGraphs()
        {
            DeinzerFileCompiler Comp = new DeinzerFileCompiler();
            var test = Comp.TranslateFile("# 9 Knotendefinitionen, A bis I knoten A knoten B knoten C knoten D knoten E # Kantendefinitionen ");
            Assert.IsNotNull(test);
        }
    }
}