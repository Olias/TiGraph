﻿using System;
using System.Collections.Generic;
using System.Text;
using TIGraphWPF.Support;
using Xunit;

namespace XUnitTestProject
{
    class DeinzerFileCompilerTest
    {
        DeinzerFileCompiler Comp = new DeinzerFileCompiler();
        [Fact]
        public void DotConvert()
        {
            var test = Comp.TranslateFile("# 9 Knotendefinitionen, A bis I knoten A knoten B knoten C knoten D knoten E # Kantendefinitionen kante B C kante B E kante A B");
            Assert.NotNull(test);
        }
    }
}
