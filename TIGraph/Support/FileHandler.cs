﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGraph.Support
{
    public class FileHandler
    {
        Windows.Storage.StorageFolder storageFolder =
                Windows.Storage.ApplicationData.Current.LocalFolder;
        private async Task<Windows.Storage.StorageFile> FileSelectorAsync()
        {
            var picker = new Windows.Storage.Pickers.FileOpenPicker();
            picker.ViewMode = Windows.Storage.Pickers.PickerViewMode.Thumbnail;
            picker.SuggestedStartLocation =
                Windows.Storage.Pickers.PickerLocationId.PicturesLibrary;
            picker.FileTypeFilter.Add(".txt");

            Windows.Storage.StorageFile file = await picker.PickSingleFileAsync();
            if (file != null)// Application now has read/write access to the picked file
            {
                return file;
            }
            return null;
        }
        public async Task<string> SelectAndReadFile() => await ReadFile(await FileSelectorAsync());
        public async Task CreateFile(string filename)
        {
            // Create sample file; replace if exists.            
            Windows.Storage.StorageFile sampleFile =
                await storageFolder.CreateFileAsync(filename,
                    Windows.Storage.CreationCollisionOption.ReplaceExisting);
        }

        public async Task<string> ReadTextFile(string fileName) => await Windows.Storage.FileIO.ReadTextAsync(await storageFolder.GetFileAsync(fileName));

        private async Task<string> ReadFile(Windows.Storage.StorageFile file) => await Windows.Storage.FileIO.ReadTextAsync(file);

        public async Task WriteFile(string fileName, string content)
        {
            Windows.Storage.StorageFolder storageFolder =
    Windows.Storage.ApplicationData.Current.LocalFolder;
            Windows.Storage.StorageFile sampleFile = await storageFolder.GetFileAsync(fileName);
            await Windows.Storage.FileIO.WriteTextAsync(sampleFile, content);

        }



        public async Task WritePicker(string content)
        {
            var savePicker = new Windows.Storage.Pickers.FileSavePicker();
            savePicker.SuggestedStartLocation =
        Windows.Storage.Pickers.PickerLocationId.DocumentsLibrary;
            // Dropdown of file types the user can save the file as
            savePicker.FileTypeChoices.Add("Plain Text", new List<string>() { ".dot" });
            // Default file name if the user does not type one in or select a file to replace
            savePicker.SuggestedFileName = "New Document";
            Windows.Storage.StorageFile file = await savePicker.PickSaveFileAsync();
            if (file != null)
            {
                // Prevent updates to the remote version of the file until
                // we finish making changes and call CompleteUpdatesAsync.
                Windows.Storage.CachedFileManager.DeferUpdates(file);
                // write to file
                await Windows.Storage.FileIO.WriteTextAsync(file, content);
                // Let Windows know that we're finished changing the file so
                // the other app can update the remote version of the file.
                // Completing updates may require Windows to ask for user input.
                Windows.Storage.Provider.FileUpdateStatus status =
                    await Windows.Storage.CachedFileManager.CompleteUpdatesAsync(file);
                if (status == Windows.Storage.Provider.FileUpdateStatus.Complete)
                {
                }
                else
                {
                }
            }
            else
            {
            }
        }
    }
}
