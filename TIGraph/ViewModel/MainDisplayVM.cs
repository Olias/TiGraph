﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using TIGraph.Support;
using Windows.UI.Popups;
using Windows.UI.Xaml;

namespace TIGraph.ViewModel
{
    class MainDisplayVM:BaseViewModel
    {
        public FileHandler FileHandler { get; set; }
        public DeinzerFileCompiler Comp = new DeinzerFileCompiler();
        public string MainTextBoxText { get {
                return _mainTextBoxText;
            }
            set { SetProperty(ref _mainTextBoxText, value); }
        }
    private string _mainTextBoxText;

        public MainDisplayVM()
        {
            FileHandler = new FileHandler();
            Initalize();
        }
        

        private async void Initalize()//TODO Put in Model
        {
            MainTextBoxText = await FileHandler.SelectAndReadFile();
            var graph=Comp.TranslateFile(MainTextBoxText);

            var test=graph.BreitensucheEinGraph();

            await FileHandler.WritePicker(graph.ConvertToDot());


        }
        public void StartButtonTapped(object sender, RoutedEventArgs e)
        {
            Initalize();
            Comp.TranslateFile(MainTextBoxText);
        }

        public ICommand Start { get; private set; }


    }

}
