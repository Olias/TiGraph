﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace TIGraph.Model.GraphModels
{
    public class DeinzerKnoten
    {
        public string Name { get; set; }
        public int Degree => Kanten.Count;
        public List<DeinzerKante> Kanten { get; set; } = new List<DeinzerKante>();

        public DeinzerKnoten(string _name)
        {
            Name = _name;
        }
        public DeinzerKnoten(string _name, List<DeinzerKante> Kanten)
        {
            Name = _name;
            this.Kanten = Kanten;
        }

        public int InputDegree
        {
            get
            {
                var temp = 0;
                foreach (var Kante in Kanten)
                {
                    if (Kante.End.Name==Name)
                    {
                        temp++;
                    }
                }
                return temp;
            }
        }
        public int OutputDegree
        {
            get
            {
                var temp = 0;
                foreach (var Kante in Kanten)
                {
                    if (Kante.Start.Name == Name)
                    {
                        temp++;
                    }
                }
                return temp;
            }
        }

        public IEnumerable<DeinzerKnoten> getNeighbours()
        {
            foreach (var k in Kanten)
            {
                if (k.End.Name == Name)
                    yield return k.Start;
                else
                
                    yield return k.End;
                
            }
        }

    }
}
