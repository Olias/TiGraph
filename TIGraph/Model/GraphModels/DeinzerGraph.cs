﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using TIGraph.Support;

namespace TIGraph.Model.GraphModels
{
    public class DeinzerGraph
    {
        public List<DeinzerKnoten> Knoten { get; set; }
        public List<DeinzerKante> Kanten { get; set; }
        public bool Directed { get; set; } = false;

        public DeinzerGraph()
        {
            Knoten = new List<DeinzerKnoten>();
            Kanten = new List<DeinzerKante>();
        }

        public DeinzerGraph(ConcurrentBag<DeinzerKnoten> _knoten, ConcurrentBag<DeinzerKante> _kanten)
        {
            Knoten = new List<DeinzerKnoten>();
            Kanten = new List<DeinzerKante>();
            foreach (var item in _knoten)
            {
                Knoten.Add(item);
            }
            foreach (var item in _kanten)
            {
                Kanten.Add(item);
            }
        }

        public DeinzerGraph(List<DeinzerKnoten> _knoten, List<DeinzerKante> _kanten)
        {
            Knoten = new List<DeinzerKnoten>();
            Kanten = new List<DeinzerKante>();
            foreach (var item in _knoten)
            {
                Knoten.Add(item);
            }
            foreach (var item in _kanten)
            {
                Kanten.Add(item);
            }
        }

        public string ConvertToDot()
        {
            //Kanten.Sort();
            return Pre + Mid + After;
        }

        private string Pre
        {
            get
            {
                if (Directed)
                {
                    return "digraph {";
                }
                else
                {
                    return "graph {";
                }
            }
        }

        private string Mid
        {
            get
            {

                var temp = "";
                foreach (var item in Kanten)
                {
                    if (Directed)
                    {
                        temp += MidElement(item, "->");
                    }
                    else
                        temp += MidElement(item, "--");
                }
                return temp;
            }
        }

        private string After => "}";

        private string MidElement(DeinzerKante kante, string seperator)
        {
            if (kante.Value == -1)
            {
                return kante.Start.Name + seperator + kante.End.Name;
            }
            else
                return kante.Start.Name + seperator + kante.End.Name + "[label=\"" + kante.Value + "\"];";
        }

        public bool UndirectedCyrcleSearch()
        {
            var tempInitList = Knoten;
            var tempint = tempInitList.Count;
            var tempout = new List<DeinzerKnoten>
            {
                tempInitList.First()
            };
            for (int i = 0; i < tempint; i++)
            {
                if (tempout.Count <= i)
                {
                    var temp = 0;
                    do
                    {
                        if (tempout.Find(a => a.Name == Knoten[temp].Name) == null)
                        {
                            tempout.Add(Knoten[temp]);
                        }
                        temp++;
                    } while (tempout.Count != i + 1);
                }
                var index = tempout[i];
                foreach (var item in tempout[i].Kanten)
                {
                    if (item.Start!=index)
                    {
                        if (tempout.Contains(item.Start))
                        {
                            return true;
                        }
                        tempout.Add(item.Start);
                        var _remove = tempInitList.FindIndex(a => a == item.Start);                        
                        tempInitList[_remove].Kanten.Remove(item);
                    }
                    else
                    {
                        if (tempout.Contains(item.End))
                        {
                            return true;
                        }
                        tempout.Add(item.End);
                        var _remove = tempInitList.FindIndex(a => a == item.End);
                        tempInitList[_remove].Kanten.Remove(item);
                    }
                }
                    
                tempInitList.Remove(tempout[i]);
            }
            return false;
        }

        public bool BreitensucheEinGraph()
        {
            var tempInitList = new List<DeinzerKnoten>(Knoten); 
            var tempint = tempInitList.Count;
            var tempout = new List<DeinzerKnoten>
            {
                tempInitList.First()
            };
            for (int i = 0; i < tempint; i++)
            {
                if (tempout.Count <= i)
                {
                    return false;
                    //var temp = 0;
                    //do
                    //{
                    //    if (tempout.Find(a => a.Name == Knoten[temp].Name) == null)
                    //    {
                    //        tempout.Add(Knoten[temp]);
                    //    }
                    //    temp++;
                    //} while (tempout.Count != i+1);
                }
                var index = tempout[i];
                foreach (var item in tempout[i].Kanten)
                {
                    if (item.Start != index)
                    {
                        tempout.Add(item.Start);
                        var _remove = tempInitList.FindIndex(a => a == item.Start);
                        tempInitList[_remove].Kanten.Remove(item);
                    }
                    else
                    {
                        tempout.Add(item.End);
                        var _remove = tempInitList.FindIndex(a => a == item.End);
                        tempInitList[_remove].Kanten.Remove(item);
                    }
                }

                tempInitList.Remove(tempout[i]);
            }            
            return true;
        }
        /// <summary>
        /// returns a list with the points in order first visted
        /// </summary>
        /// <returns></returns>
        public List<DeinzerKnoten> UndirectedCyrcleSearchWithColor()
        {
            var tempInitList = Knoten;
            var tempint = tempInitList.Count;
            
            var tempout = new List<DeinzerKnoten>
            {
                tempInitList.First()
            };
            for (int i = 0; i < tempint; i++)
            {
                if (tempout.Count <= i)
                {
                    var temp = 0;
                    do
                    {
                        if (tempout.Find(a => a.Name == Knoten[temp].Name) == null)
                        {
                            tempout.Add(Knoten[temp]);
                        }
                        temp++;
                    } while (tempout.Count != i + 1);
                }
                var index = tempout[i];
                foreach (var item in tempout[i].Kanten)
                {
                    if (item.Start != index)
                    {
                        if (tempout.Contains(item.Start))
                        {
                            return tempout;
                        }
                        tempout.Add(item.Start);
                        var _remove = tempInitList.FindIndex(a => a == item.Start);
                        tempInitList[_remove].Kanten.Remove(item);
                    }
                    else
                    {
                        if (tempout.Contains(item.End))
                        {
                            return tempout;
                        }
                        tempout.Add(item.End);
                        var _remove = tempInitList.FindIndex(a => a == item.End);
                        tempInitList[_remove].Kanten.Remove(item);
                    }
                }

                tempInitList.Remove(tempout[i]);
            }
            return tempout;
        }
        public void test(List<DeinzerKante> input)
        {
            
        }
        
        bool checkEvenDegree()
        {
            bool temp = false;

            if (Directed == true)
            {
                foreach (DeinzerKnoten k in Knoten)
                {
                    if (k.InputDegree == k.OutputDegree)
                        temp = true;
                    else
                        return false;
                }
                return temp;
            }
            else
            {
                foreach (DeinzerKnoten k in Knoten)
                {
                    if (k.Degree % 2 == 0)
                        temp = true;
                    else
                        return false;
                }
                return temp;
            }
        }

        public IEnumerable<DeinzerKnoten> BreadthFirstSearch(DeinzerKnoten k)
        {

            Queue<DeinzerKnoten> deinzerKnotenQueue = new Queue<DeinzerKnoten>(); 
            HashSet<DeinzerKnoten> visitedDeinzerKnoten = new HashSet<DeinzerKnoten>();

            deinzerKnotenQueue.Enqueue(k);

            while (deinzerKnotenQueue.Count != 0)
            {
                var currentNode = deinzerKnotenQueue.Dequeue();
                if(visitedDeinzerKnoten.Contains(currentNode))
                    continue;

                visitedDeinzerKnoten.Add(currentNode);
                yield return currentNode;

                deinzerKnotenQueue.EnqueueRange(currentNode.getNeighbours().Except(visitedDeinzerKnoten));
            }

            
        }
    }
}
        
    




