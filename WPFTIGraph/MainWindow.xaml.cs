﻿using QuickGraph;
using QuickGraph.Algorithms.Search;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TIGraph.Model.GraphModels;
using TIGraph.Support;

namespace WPFTIGraph
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            FileHandler = new FileHandler();
            Initalize();
            
        }

        public FileHandler FileHandler { get; set; }
        public DeinzerFileCompiler Comp = new DeinzerFileCompiler();
        public string MainTextBoxText { get; set; }        


        private async void Initalize()//TODO Put in Model
        {/*Comp.testquickgraph();*/
            MainTextBoxText = await FileHandler.ReadFile();
            var graph = Comp.TranslateFile(MainTextBoxText);
            DeinzerGraph graph2;
            if (graph.Directed)
            {
                if (graph.Kanten.First().Value==-1)
                {
                    var test = graph.TransformToAdjacencyGraph();

                    graph2 = new DeinzerGraph(test);
                }
                else
                {
                    var test = graph.TransformToAdjacencyGraphWeighted();

                    graph2 = new DeinzerGraph(test);
                }
            }
            else
            {
                if (graph.Kanten.First().Value == -1)
                {
                    var test = graph.TransformToUndirectedGraph();

                    graph2 = new DeinzerGraph(test);
                }
                else
                {
                    var test = graph.TransformToUndirectedGraphWeighted();

                    graph2 = new DeinzerGraph(test);
                }
            }


            await FileHandler.WritePicker(graph2.ConvertToDot());

            
        }
        public void StartButtonTapped(object sender, RoutedEventArgs e)
        {
            Initalize();
            Comp.TranslateFile(MainTextBoxText);
        }

        public void testc(QuickGraph.UndirectedGraph<string, Edge<string>>inputgraph)
        {
            //var g = new AdjacencyGraph<string, Edge<string>>();
            //var dfs = new DepthFirstSearchAlgorithm<string, Edge<string>>(g);
            //var dtf = new DepthFirstSearchAlgorithm<string, Edge<string>>(inputgraph);
        }
    }
}
