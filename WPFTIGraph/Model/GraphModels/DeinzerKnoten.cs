﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGraph.Model.GraphModels
{
    public class DeinzerKnoten
    {
        public string Name { get; set; }

        public DeinzerKnoten(string _name)
        {
            Name = _name;
        }
    }
}
