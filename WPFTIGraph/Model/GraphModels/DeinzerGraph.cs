﻿using QuickGraph;
using QuickGraph.Graphviz;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGraph.Model.GraphModels
{
    public class DeinzerGraph
    {
        public List<DeinzerKnoten> Knoten { get; set; }
        public List<DeinzerKante> Kanten { get; set; }
        public bool Directed { get; set; } = false;
        public DeinzerGraph()
        {
            Knoten = new List<DeinzerKnoten>();
            Kanten = new List<DeinzerKante>();
        }

        public DeinzerGraph(ConcurrentBag<DeinzerKnoten>_knoten, ConcurrentBag<DeinzerKante> _kanten)
        {
            Knoten = new List<DeinzerKnoten>();
            Kanten = new List<DeinzerKante>();
            foreach (var item in _knoten)
            {
                Knoten.Add(item);
            }
            foreach (var item in _kanten)
            {
                Kanten.Add(item);
            }
            //if (Kanten.First().Value!=-1)
            //{
            //    Directed = true;
            //}
        }

        public DeinzerGraph(QuickGraph.AdjacencyGraph<string, Edge<string>> Graph)
        {Knoten = new List<DeinzerKnoten>();
            Kanten = new List<DeinzerKante>();
            Directed = true;
            foreach (var item in Graph.Vertices)
            {
                
                Knoten.Add(new DeinzerKnoten(item.ToString()));
            }
            foreach (var item in Graph.Edges)
            {
                Kanten.Add(new DeinzerKante(item.Source,item.Target));
            }
        }
        public DeinzerGraph(QuickGraph.AdjacencyGraph<string, TaggedEdge<string,int>> Graph)
        {
            Knoten = new List<DeinzerKnoten>();
            Kanten = new List<DeinzerKante>();
            Directed = true;
            foreach (var item in Graph.Vertices)
            {
                Knoten.Add(new DeinzerKnoten(item.ToString()));
            }
            foreach (var item in Graph.Edges)
            {
                Kanten.Add(new DeinzerKante(item.Source, item.Target, item.Tag));
            }
        }
        public DeinzerGraph(QuickGraph.UndirectedGraph<string, Edge<string>> Graph)
        {
            Knoten = new List<DeinzerKnoten>();
            Kanten = new List<DeinzerKante>();
            foreach (var item in Graph.Vertices)
            {
                Knoten.Add(new DeinzerKnoten(item.ToString()));
            }
            foreach (var item in Graph.Edges)
            {
                Kanten.Add(new DeinzerKante(item.Source, item.Target));
            }
        }
        public DeinzerGraph(QuickGraph.UndirectedGraph<string, TaggedEdge<string, int>> Graph)
        {
            Knoten = new List<DeinzerKnoten>();
            Kanten = new List<DeinzerKante>();
            foreach (var item in Graph.Vertices)
            {
                Knoten.Add(new DeinzerKnoten(item.ToString()));
            }
            foreach (var item in Graph.Edges)
            {
                Kanten.Add(new DeinzerKante(item.Source, item.Target, item.Tag));
            }
        }
        public string ConvertToDot()
        {
            //Kanten.Sort();
            return Pre + Mid + After;
        }
        private string Pre
        {
            get
            {
                if (Directed)
                {
                    return "digraph {";
                }
                else
                {
                    return "graph {";
                }
            }
        }

        private string Mid { get
            {
                
                var temp = "";
                foreach (var item in Kanten)
                {
                    if (Directed)
                    {
                        temp+=MidElement(item, "->");
                    }
                    else
                        temp+= MidElement(item, "--");
                }
                return temp;
            } }
        private string After => "}";
        
        private string MidElement(DeinzerKante kante, string seperator)
        {
            if (kante.Value == -1)
            {
                return kante.Start + seperator + kante.End;
            }
            else
                return kante.Start + seperator + kante.End + "[label=\"" + kante.Value + "\"];";
        }
        public QuickGraph.AdjacencyGraph<string, Edge<string>> TransformToAdjacencyGraph()
        {
            var graph = new AdjacencyGraph<string, Edge<string>>();
            foreach (var item in Knoten)
            {
                graph.AddVertex(item.Name);
            }
            foreach (var item in Kanten)
            {
                        graph.AddEdge(new Edge<string>(item.Start, item.End));
                    
                
            }
            return graph;
        }
        public QuickGraph.AdjacencyGraph<string, TaggedEdge<string,int>> TransformToAdjacencyGraphWeighted()
        {
            var graph = new AdjacencyGraph<string, TaggedEdge<string, int>>();
            foreach (var item in Knoten)
            {
                graph.AddVertex(item.Name);
            }
            foreach (var item in Kanten)
            {
                    graph.AddEdge(new TaggedEdge<string, int>(item.Start, item.End, item.Value));             

            }
            return graph;
        }
        public QuickGraph.UndirectedGraph<string, Edge<string>> TransformToUndirectedGraph()
        {
            var undiGraph = new UndirectedGraph<string, Edge<string>>();
            foreach (var item in Knoten)
            {
                undiGraph.AddVertex(item.Name);
            }
            foreach (var item in Kanten)
            {
                        undiGraph.AddEdge(new Edge<string>(item.Start, item.End));
                    
                
            }
            return undiGraph;
        }
        public QuickGraph.UndirectedGraph<string, TaggedEdge<string,int>> TransformToUndirectedGraphWeighted()
        {
            var undiGraph = new UndirectedGraph<string, TaggedEdge<string,int>>();
            foreach (var item in Knoten)
            {
                undiGraph.AddVertex(item.Name);
            }
            foreach (var item in Kanten)
            {
                    undiGraph.AddEdge(new TaggedEdge<string, int>(item.Start, item.End, item.Value));
            }
            return undiGraph;
        }



        //private void PrintShortestPath(string @from, string to)
        //{
        //    var edgeCost = AlgorithmExtensions.GetIndexer(_costs);
        //    var tryGetPath = _graph.ShortestPathsDijkstra(edgeCost, @from);

        //    IEnumerable<Edge<string>> path;
        //    if (tryGetPath(to, out path))
        //    {
        //        PrintPath(@from, to, path);
        //    }
        //    else
        //    {
        //        Console.WriteLine("No path found from {0} to {1}.");
        //    }
        //}
    }
}
