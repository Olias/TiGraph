﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGraph.Model.GraphModels
{
    public class DeinzerKante:IComparable<DeinzerKante>
    {
        public string Start { get; set; }
        public string End { get; set; }
        public int Value { get; set; } = -1;

        public DeinzerKante(string _start, string _end, int _value)
        {
            Start = _start;
            End = _end;
            Value = _value;
        }
        public DeinzerKante(string _start, string _end)
        {
            Start = _start;
            End = _end;
        }
        public int CompareTo(DeinzerKante other)
        {
            return String.Compare(this.Start, other.Start);
        }
    }
}
