﻿using QuickGraph;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TIGraph.Model.GraphModels;

namespace TIGraph.Support
{
    public class DeinzerFileCompiler
    {
        public ConcurrentBag<DeinzerKnoten> Nodes { get; set; }
        public ConcurrentBag<DeinzerKante> Edges { get; set; }
        char[] Signs = { '#'};
        string[] stringSeparators = new string[] { "\r\n" };
        public DeinzerFileCompiler()
        {
            Nodes = new ConcurrentBag<DeinzerKnoten>();
            Edges = new ConcurrentBag<DeinzerKante>();
        }

        public DeinzerGraph TranslateFile(string inputString)
        {
            inputString=RemoveBetween(inputString,"#", "\r\n");
            var inputStrings=inputString.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
            Parallel.ForEach(inputStrings, singleString => {
                FilterObjects(singleString);
            });

            return new DeinzerGraph(Nodes,Edges);
        }
        string RemoveBetween(string s, string begin, string end)
        {
            Regex regex = new Regex(string.Format("\\{0}.*?\\{1}", begin, end));
            return regex.Replace(s, string.Empty);
        }
        private void FilterObjects(string inputString)
        {
            inputString = inputString.Replace("\r\n","");
            var words = inputString.Split(' ');
            if (words.Count() != 0)
            {
                switch (words[0])
                {
                    case "knoten":
                        {
                            Nodes.Add(new DeinzerKnoten(words[1]));
                            break;
                        }
                    case "kante":
                        {
                            
                            if (words.Count()==3)
                            {
                                Edges.Add(new DeinzerKante(words[1], words[2]));
                            }
                            else if (words.Count() == 4)
                            {
                                try
                                {
                                    Edges.Add(new DeinzerKante(words[1], words[2],int.Parse(words[3])));                                }
                                catch (Exception)
                                {
                                }
                                
                            }
                            break;
                        }
                    default:
                        break;
                }
            }
        }
        //public void testsatsuma()
        //{
        //    CustomGraph g = new CustomGraph();
        //    Node a = g.AddNode();
        //    Node b = g.AddNode();
        //    Node c = g.AddNode();
        //    Arc ab = g.AddArc(a, b, Directedness.Directed);
        //    Arc bc = g.AddArc(b, c, Directedness.Undirected);
        //    var ttestt=g.Arcs();
        //    var testasa=g.Nodes();
        //}

        //public void testquickgraph()
        //{
        //    var graph = new AdjacencyGraph<int, Edge<int>>();
        //    int v1 = 1;
        //    int v2 = 2;
        //    var e1 = new TaggedEdge<int, string>(v1, v2, "hello");
        //    var e2 = new Edge<int>(v1, v2);
        //    graph.AddVertex(v1);
        //    graph.AddVertex(v2);
        //    graph.AddEdge(e2);
        //    var test = graph.Edges;
        //}
    }
}
